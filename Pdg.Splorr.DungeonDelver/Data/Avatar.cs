﻿using Newtonsoft.Json.Linq;
using Pdg.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pdg.Splorr.DungeonDelver
{
    public struct Avatar
    {
        private class Properties
        {
            public const string AtlasX = "atlasX";
            public const string AtlasY = "atlasY";
            public const string MapX = "mapX";
            public const string MapY = "mapY";
            public const string Command = "command";
        }
        public XY AtlasPosition;
        public XY MapPosition;
        public AvatarCommand? Command;
        internal Avatar SetAtlasPosition(XY atlas)
        {
            return Create(atlas, MapPosition, Command);
        }
        internal Avatar SetMapPosition(XY map)
        {
            return Create(AtlasPosition, map, Command);
        }
        internal Avatar SetCommand(AvatarCommand command)
        {
            return Create(AtlasPosition, MapPosition, command);
        }
        internal JObject Export()
        {
            JObject export = new JObject();
            export[Properties.AtlasX] = AtlasPosition.X;
            export[Properties.AtlasY] = AtlasPosition.Y;
            export[Properties.MapX] = MapPosition.X;
            export[Properties.MapY] = MapPosition.Y;
            if(Command.HasValue)
            {
                export[Properties.Command] = (int)Command.Value;
            }
            return export;
        }
        internal static Avatar Create(XY atlas, XY map, AvatarCommand? command)
        {
            return new Avatar() { AtlasPosition = atlas, MapPosition = map, Command = command };
        }
        internal static Avatar Create(JObject import)
        {
            return Create(
                XY.Create(
                    import[Properties.AtlasX].ToObject<int>(), 
                    import[Properties.AtlasY].ToObject<int>()), 
                XY.Create(
                    import[Properties.MapX].ToObject<int>(), 
                    import[Properties.MapY].ToObject<int>()),
                import[Properties.Command] != null ? (AvatarCommand?)import[Properties.AtlasX].ToObject<int>() : (AvatarCommand?)null);
        }
    }
}
