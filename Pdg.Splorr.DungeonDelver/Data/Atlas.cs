﻿using Newtonsoft.Json.Linq;
using Pdg.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pdg.Splorr.DungeonDelver
{
    public class Atlas<TGame>:Board<XY, Map<TGame>> where TGame:IGame<TGame>
    {
        private class Properties
        {
            public const string X = "x";
            public const string Y = "y";
            public const string Map = "map";
        }
        internal const int Columns = 8;
        internal const int Rows = 8;
        internal const int ColumnLeft = 0;
        internal const int RowTop = 0;
        internal const int ColumnRight = Columns - 1;
        internal const int RowBottom = Rows - 1;
        internal TGame Game { get; set; }
        internal JArray Export()
        {
            JArray export = new JArray();
            foreach(var location in Locations)
            {
                JObject mapObject = new JObject();
                mapObject[Properties.X] = location.X;
                mapObject[Properties.Y] = location.Y;
                mapObject[Properties.Map] = this[location].Export();
                export.Add(mapObject);
            }
            return export;
        }
        internal static bool Validator(XY location)
        {
            return location.X >= 0 && location.Y >= 0 && location.X < Columns && location.Y < Rows;
        }
        internal static Map<TGame> Defaulter()
        {
            return null;
        }
        public Atlas(TGame game) : base(Validator, Defaulter)
        {
            Game = game;
            for(int column=0;column<Columns;++column)
            {
                for(int row=0;row<Rows;++row)
                {
                    this[XY.Create(column, row)] = new Map<TGame>(this);
                }
            }
        }
        public Atlas(TGame game, JArray import) : base(Validator, Defaulter)
        {
            Game = game;
            foreach(var token in import)
            {
                var item = token as JObject;
                XY location = XY.Create(item[Properties.X].ToObject<int>(), item[Properties.Y].ToObject<int>());
                Map<TGame> map = new Map<TGame>(this, item[Properties.Map] as JObject);
                this[location] = map;
            }
        }
    }
}
