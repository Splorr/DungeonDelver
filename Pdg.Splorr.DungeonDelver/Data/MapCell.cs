﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;

namespace Pdg.Splorr.DungeonDelver
{
    public class MapCell<TGame> where TGame : IGame<TGame>
    {
        private class Properties
        {
            public const string Terrain = "terrain";
            public const string Creature = "creature";
            public const string Item = "item";
        }
        internal Map<TGame> Map { get; set; }
        internal Atlas<TGame> Atlas { get { return Map.Atlas; } }
        internal TGame Game { get { return Atlas.Game; } }
        public TerrainInstance<TGame> Terrain;
        public ItemInstance<TGame> Item;
        public CreatureInstance<TGame> Creature;
        private MapCell() { }
        internal MapCell(Map<TGame> map)
        {
            Map = map;
        }

        internal JObject Export()
        {
            JObject export = new JObject();
            if(Terrain!=null)
            {
                export[Properties.Terrain] = Terrain.Export();
            }
            if(Item!=null)
            {
                export[Properties.Item] = Item.Export();
            }
            if (Creature!=null)
            {
                export[Properties.Creature] = Creature.Export();
            }
            return export;
        }
    }
}
