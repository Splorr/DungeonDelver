﻿using Newtonsoft.Json.Linq;
using Pdg.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pdg.Splorr.DungeonDelver
{
    public class Map<TGame> : Board<XY, MapCell<TGame>> where TGame : IGame<TGame>
    {
        private class Properties
        {
            public const string X = "x";
            public const string Y = "y";
            public const string Cell = "cell";
            public const string Cells = "cells";
        }

        public const int Columns = 24;
        internal const int Rows = 16;
        internal const int ColumnLeft = 0;
        internal const int ColumnRight = Columns - 1;
        internal const int RowTop = 0;
        internal const int RowBottom = Rows - 1;
        internal const int CorridorWidth = Columns / 4;
        internal const int CorridorHeight = Rows / 4;
        internal const int CorridorLeft = Columns / 2 - CorridorWidth / 2;
        internal const int CorridorTop = Rows / 2 - CorridorHeight / 2;
        internal const int CorridorRight = CorridorLeft + CorridorWidth - 1;
        internal const int CorridorBottom = CorridorTop + CorridorHeight - 1;

        internal void TransformTerrain(Func<TerrainInstance<TGame>, bool> condition, TerrainType newTerrainType, Action<TerrainInstance<TGame>> postTransform)
        {
            foreach(var location in Locations)
            {
                var cell = this[location];
                if(cell.Terrain!=null && condition(cell.Terrain))
                {
                    cell.Terrain = Game.TerrainManager[newTerrainType].Instantiate(cell);
                    postTransform(cell.Terrain);
                }
            }
        }

        internal Atlas<TGame> Atlas { get; set; }
        internal TGame Game
        {
            get
            {
                return Atlas.Game;
            }
        }
        public int Visits { get; set; }
        internal void AddVisit()
        {
            Visits++;
        }
        internal static bool Validator(XY location)
        {
            return location.X>=0 && location.Y>=0 && location.X< Columns && location.Y<Rows;
        }
        internal static MapCell<TGame> Defaulter()
        {
            return null;
        }
        internal JObject Export()
        {
            JObject export = new JObject();
            JArray cells = new JArray();
            export[Properties.Cells] = cells;
            foreach(var location in Locations)
            {
                JObject cell = new JObject();
                cell[Properties.X] = location.X;
                cell[Properties.Y] = location.Y;
                cell[Properties.Cell] = this[location].Export();
                cells.Add(cell);
            }
            return export;
        }
        public Map(Atlas<TGame> atlas) : base(Validator, Defaulter)
        {
            Atlas = atlas;
            for(var column=0;column<Columns;++column)
            {
                for(var row=0;row<Rows; ++row)
                {
                    this[XY.Create(column, row)] = new MapCell<TGame>(this);
                }
            }
        }
        public Map(Atlas<TGame> atlas, JObject import) : base(Validator, Defaulter)
        {
            Atlas = atlas;
        }

        internal static XY Wrap(XY xy)
        {
            int x = xy.X % Columns;
            int y = xy.Y % Rows;
            return XY.Create(x < 0 ? x + Columns : x, y < 0 ? y + Rows : y);
        }
    }
}
