﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pdg.Splorr.DungeonDelver
{
    public enum DifficultyLevel
    {
        Easy,
        Medium,
        Hard
    }
}
