﻿using Pdg.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pdg.Splorr.DungeonDelver
{
    public class ItemDescriptor<TGame> where TGame:IGame<TGame>
    {
        internal ItemType ItemType
        {
            get; set;
        }
        internal Func<MapCell<TGame>, ItemInstance<TGame>> Instantiate
        {
            get;set;
        }
        public Func<ItemInstance<TGame>, byte> GetCharacter
        {
            get; set;
        }
        internal Func<CreatureInstance<TGame>, ItemInstance<TGame>, bool> CanPickUp
        {
            get; set;
        }
        internal Action<CreatureInstance<Game>, ItemInstance<Game>> OnPickUp
        {
            get;set;
        }
        internal Action<CreatureInstance<TGame>, ItemInstance<TGame>> OnBump
        {
            get; set;
        }

        internal int SpawnCount { get; set; }
        public ItemDescriptor() 
        {
        }
    }
}
