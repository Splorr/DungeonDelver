﻿using Pdg.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pdg.Splorr.DungeonDelver
{
    public class TerrainDescriptor<TGame> where TGame:IGame<TGame>
    {
        internal TerrainType TerrainType
        {
            get; set;
        }

        public Func<TerrainInstance<TGame>, byte> GetCharacter
        {
            get; set;
        }

        internal Func<TerrainInstance<TGame>, CreatureInstance<TGame>,bool> CanSpawn
        {
            get; set;
        }

        internal Func<TerrainInstance<TGame>, CreatureInstance<TGame>, bool> CanEnter
        {
            get; set;
        }

        internal Func<TerrainInstance<TGame>, ItemInstance<TGame>, bool> CanPlace
        {
            get; set;
        }
        internal Func<MapCell<TGame>, TerrainInstance<TGame>> Instantiate
        {
            get; set;
        }
        public TerrainDescriptor() 
        {
        }
    }
}
