﻿using Pdg.Common;
using System;

namespace Pdg.Splorr.DungeonDelver
{
    public class TerrainManager<TGame> : Manager<TerrainType, TerrainDescriptor<TGame>> where TGame : IGame<TGame>
    {
        internal static TerrainDescriptor<TGame> Factory(TerrainType type)
        {
            TerrainDescriptor<TGame> result = new TerrainDescriptor<TGame>();
            result.TerrainType = type;
            Func<TerrainInstance<TGame>, byte> doorCharacter = i => 
            {
                switch(i.KeyColor)
                {
                    case KeyColor.Green:
                        return 0x89;
                    case KeyColor.Yellow:
                        return 0x99;
                    case KeyColor.Blue:
                        return 0xA9;
                    case KeyColor.Red:
                        return 0xB9;
                    case KeyColor.White:
                        return 0xC9;
                    case KeyColor.Cyan:
                        return 0xD9;
                    case KeyColor.Magenta:
                        return 0xE9;
                    case KeyColor.Orange:
                        return 0xF9;
                    default:
                        throw new NotImplementedException();
                }
            };

            switch(type)
            {
                case TerrainType.EastDoor:
                    result.GetCharacter = doorCharacter;
                    result.CanSpawn = (t, c) => false;
                    result.CanEnter = (t,c) => 
                    {
                        if(c.KeyInventory!=null && c.KeyInventory.ContainsKey(t.KeyColor.Value) && c.KeyInventory[t.KeyColor.Value]>0)
                        {
                            c.KeyInventory[t.KeyColor.Value]--;
                            t.Map.TransformTerrain(input => input.TerrainType == TerrainType.EastDoor, TerrainType.EastEdge, x => { });
                        }
                        return false;
                    };
                    result.CanPlace = (t, i) => false;
                    result.Instantiate = (c) =>
                    {
                        TerrainInstance<TGame> instance = new TerrainInstance<TGame>(c);
                        instance.TerrainType = TerrainType.EastDoor;
                        return instance;
                    };
                    break;
                case TerrainType.EastEdge:
                    result.GetCharacter = i => 32;
                    result.CanSpawn = (t, c) => c.CreatureType == CreatureType.Tagon;
                    result.CanEnter = (t, c) => c.CreatureType == CreatureType.Tagon;
                    result.CanPlace = (t, i) => false;
                    result.Instantiate = (c) =>
                    {
                        TerrainInstance<TGame> instance = new TerrainInstance<TGame>(c);
                        instance.TerrainType = TerrainType.EastEdge;
                        return instance;
                    };
                    break;
                case TerrainType.NorthDoor:
                    result.GetCharacter = doorCharacter;
                    result.CanSpawn = (t, c) => false;
                    result.CanEnter = (t, c) =>
                    {
                        if (c.KeyInventory != null && c.KeyInventory.ContainsKey(t.KeyColor.Value) && c.KeyInventory[t.KeyColor.Value] > 0)
                        {
                            c.KeyInventory[t.KeyColor.Value]--;
                            t.Map.TransformTerrain(input => input.TerrainType == TerrainType.NorthDoor, TerrainType.NorthEdge, x => { });
                        }
                        return false;
                    };
                    result.CanPlace = (t, i) => false;
                    result.Instantiate = (c) =>
                    {
                        TerrainInstance<TGame> instance = new TerrainInstance<TGame>(c);
                        instance.TerrainType = TerrainType.NorthDoor;
                        return instance;
                    };
                    break;
                case TerrainType.NorthEdge:
                    result.GetCharacter = i => 32;
                    result.CanSpawn = (t, c) => c.CreatureType == CreatureType.Tagon;
                    result.CanEnter = (t, c) => c.CreatureType== CreatureType.Tagon;
                    result.CanPlace = (t, i) => false;
                    result.Instantiate = (c) =>
                    {
                        TerrainInstance<TGame> instance = new TerrainInstance<TGame>(c);
                        instance.TerrainType = TerrainType.NorthEdge;
                        return instance;
                    };
                    break;
                case TerrainType.Open:
                    result.GetCharacter = i => 32;
                    result.CanSpawn = (t, c) => true;
                    result.CanEnter = (t, c) => true;
                    result.CanPlace= (t, i) => 
                    {
                        switch(i.ItemType)
                        {
                            case ItemType.Key:
                            case ItemType.SmallGold:
                            case ItemType.MediumGold:
                            case ItemType.Torch:
                            case ItemType.Lantern:
                                return true;
                            default:
                                return false;
                        }
                    };
                    result.Instantiate = (c) =>
                    {
                        TerrainInstance<TGame> instance = new TerrainInstance<TGame>(c);
                        instance.TerrainType = TerrainType.Open;
                        return instance;
                    };
                    break;
                case TerrainType.OpenDeadEnd:
                    result.GetCharacter = i => 32;
                    result.CanSpawn = (t, c) => false;
                    result.CanEnter = (t, c) => true;
                    result.CanPlace = (t, i) => 
                    {
                        switch(i.ItemType)
                        {
                            case ItemType.Quest:
                            case ItemType.MediumGold:
                            case ItemType.BigGold:
                            case ItemType.Torch:
                            case ItemType.Lantern:
                            case ItemType.MagicLantern:
                                return true;
                            default:
                                return false;
                        }
                    };
                    result.Instantiate = (c) =>
                    {
                        TerrainInstance<TGame> instance = new TerrainInstance<TGame>(c);
                        instance.TerrainType = TerrainType.OpenDeadEnd;
                        return instance;
                    };
                    break;
                case TerrainType.SouthDoor:
                    result.GetCharacter = doorCharacter;
                    result.CanSpawn = (t, c) => false;
                    result.CanEnter = (t, c) =>
                    {
                        if (c.KeyInventory != null && c.KeyInventory.ContainsKey(t.KeyColor.Value) && c.KeyInventory[t.KeyColor.Value] > 0)
                        {
                            c.KeyInventory[t.KeyColor.Value]--;
                            t.Map.TransformTerrain(input => input.TerrainType == TerrainType.SouthDoor, TerrainType.SouthEdge, x => { });
                        }
                        return false;
                    };
                    result.CanPlace= (t, i) => false;
                    result.Instantiate = (c) =>
                    {
                        TerrainInstance<TGame> instance = new TerrainInstance<TGame>(c);
                        instance.TerrainType = TerrainType.SouthDoor;
                        return instance;
                    };
                    break;
                case TerrainType.SouthEdge:
                    result.GetCharacter = i => 32;
                    result.CanSpawn = (t, c) => c.CreatureType == CreatureType.Tagon;
                    result.CanEnter = (t, c) => c.CreatureType == CreatureType.Tagon;
                    result.CanPlace= (t, i) => false;
                    result.Instantiate = (c) =>
                    {
                        TerrainInstance<TGame> instance = new TerrainInstance<TGame>(c);
                        instance.TerrainType = TerrainType.SouthEdge;
                        return instance;
                    };
                    break;
                case TerrainType.Wall:
                    result.GetCharacter = i => 128;
                    result.CanSpawn = (t, c) => false;
                    result.CanEnter = (t, c) => false;
                    result.CanPlace= (t, i) => false;
                    result.Instantiate = (c) =>
                    {
                        TerrainInstance<TGame> instance = new TerrainInstance<TGame>(c);
                        instance.TerrainType = TerrainType.Wall;
                        return instance;
                    };
                    break;
                case TerrainType.WestDoor:
                    result.GetCharacter = doorCharacter;
                    result.CanSpawn = (t, c) => false;
                    result.CanEnter = (t, c) =>
                    {
                        if (c.KeyInventory != null && c.KeyInventory.ContainsKey(t.KeyColor.Value) && c.KeyInventory[t.KeyColor.Value] > 0)
                        {
                            c.KeyInventory[t.KeyColor.Value]--;
                            t.Map.TransformTerrain(input => input.TerrainType == TerrainType.WestDoor, TerrainType.WestEdge, x => { });
                        }
                        return false;
                    };
                    result.CanPlace= (t, i) => false;
                    result.Instantiate = (c) =>
                    {
                        TerrainInstance<TGame> instance = new TerrainInstance<TGame>(c);
                        instance.TerrainType = TerrainType.WestDoor;
                        return instance;
                    };
                    break;
                case TerrainType.WestEdge:
                    result.GetCharacter = i => 32;
                    result.CanSpawn = (t, c) => c.CreatureType == CreatureType.Tagon;
                    result.CanEnter = (t, c) => c.CreatureType == CreatureType.Tagon;
                    result.CanPlace= (t, i) => false;
                    result.Instantiate = (c) =>
                    {
                        TerrainInstance<TGame> instance = new TerrainInstance<TGame>(c);
                        instance.TerrainType = TerrainType.WestEdge;
                        return instance;
                    };
                    break;
                default:
                    throw new NotImplementedException();
            }
            return result;
        }
        public TerrainManager() 
            : base(Factory, x => { })
        {

        }
    }
}
