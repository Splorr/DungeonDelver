﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pdg.Splorr.DungeonDelver
{
    public enum TerrainType
    {
        Open,
        OpenDeadEnd,
        NorthEdge,
        EastEdge,
        SouthEdge,
        WestEdge,
        Wall,
        NorthDoor,
        EastDoor,
        SouthDoor,
        WestDoor
    }
}
