﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Pdg.Common;

namespace Pdg.Splorr.DungeonDelver
{
    public class IntGeneratorManager: Manager<IntGeneratorType, IWeightedGenerator<int>>
    {
        internal static IWeightedGenerator<int> Factory(IntGeneratorType type)
        {
            IWeightedGenerator<int> result = new WeightedGenerator<int>();
            switch(type)
            {
                case IntGeneratorType.Zero:
                    result.SetWeight(0, 1);
                    break;
                case IntGeneratorType.OneThird:
                    result.SetWeight(0, 2);
                    result.SetWeight(1, 1);
                    break;
                case IntGeneratorType.OneHalf:
                    result.SetWeight(0, 1);
                    result.SetWeight(1, 1);
                    break;
                case IntGeneratorType.TwoThirds:
                    result.SetWeight(0, 1);
                    result.SetWeight(1, 2);
                    break;
                case IntGeneratorType.One:
                    result.SetWeight(1, 1);
                    break;
                default:
                    throw new NotImplementedException();
            }
            return result;
        }
        public IntGeneratorManager() 
            : base(Factory, x => { })
        {
        }
    }
}
