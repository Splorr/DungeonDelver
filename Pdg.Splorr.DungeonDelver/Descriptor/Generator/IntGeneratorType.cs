﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pdg.Splorr.DungeonDelver
{
    public enum IntGeneratorType
    {
        Zero,
        OneThird,
        OneHalf,
        TwoThirds,
        One
    }
}
