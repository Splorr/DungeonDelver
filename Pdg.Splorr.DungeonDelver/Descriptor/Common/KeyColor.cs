﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pdg.Splorr.DungeonDelver
{
    public enum KeyColor
    {
        Green,
        Yellow,
        Blue,
        Red,
        White,
        Cyan,
        Magenta,
        Orange,
        Rainbow
    }
}
