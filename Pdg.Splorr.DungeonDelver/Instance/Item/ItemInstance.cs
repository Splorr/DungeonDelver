﻿using Pdg.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;

namespace Pdg.Splorr.DungeonDelver
{
    public class ItemInstance<TGame> : CommonInstance<TGame> where TGame : IGame<TGame>
    {
        private class Properties
        {
            public const string Gold = "gold";
            public const string DurabilityType = "durabilityType";
            public const string Durability = "durability";
            public const string ItemType = "itemType";
            public const string KeyColor = "keyColor";
        }
        protected override MapCell<TGame> GetMapCell()
        {
            var result =  base.GetMapCell();
            if(result==null && Creature !=null)
            {
                result = Creature.MapCell;
            }
            return result;
        }
        internal CreatureInstance<TGame> Creature { get; set; }
        internal int Gold { get; set; }
        internal DurabilityType? DurabilityType { get; set; }
        internal int Durability { get; set; }
        internal ItemType ItemType
        {
            get; set;
        }
        internal KeyColor? KeyColor
        {
            get; set;
        }
        public ItemDescriptor<TGame> Descriptor
        {
            get
            {
                return Game.ItemManager[ItemType];
            }
        }
        internal JToken Export()
        {
            JObject export = new JObject();
            export[Properties.Durability] = Durability;
            if (DurabilityType.HasValue)
            {
                export[Properties.DurabilityType] = (int)DurabilityType.Value;
            }
            export[Properties.Gold] = Gold;
            export[Properties.ItemType] = (int)ItemType;
            if (KeyColor.HasValue)
            {
                export[Properties.KeyColor] = (int)KeyColor.Value;
            }
            return export;
        }

        public ItemInstance(MapCell<TGame> mapCell) 
            : base(mapCell)
        {
        }
        public ItemInstance(CreatureInstance<TGame> creature)
            :base(null)
        {
            Creature = creature;
        }
    }
}
