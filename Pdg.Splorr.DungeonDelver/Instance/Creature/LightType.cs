﻿using Pdg.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;

namespace Pdg.Splorr.DungeonDelver
{
    public enum LightType
    {
        Torch,
        Lantern,
        MagicLantern
    }
}
