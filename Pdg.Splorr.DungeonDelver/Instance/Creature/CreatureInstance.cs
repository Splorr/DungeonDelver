﻿using Pdg.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;

namespace Pdg.Splorr.DungeonDelver
{
    public class CreatureInstance<TGame> : CommonInstance<TGame> where TGame : IGame<TGame>
    {
        private class Properties
        {
            public const string Gold = "gold";
            public const string CreatureType = "creatureType";
            public const string LightType = "lightType";
            public const string KeyInventory = "keyInventory";
            public const string Durabilities = "durabilities";
            public const string QuestItems = "questItems";
            public const string TurnCounter = "turnCounter";
        }
        public int Gold { get; set; }
        internal CreatureType CreatureType
        {
            get; set;
        }
        public CreatureDescriptor<TGame> Descriptor
        {
            get
            {
                return Game.CreatureManager[CreatureType];
            }
        }
        public LightType? LightType
        {
            get;set;
        }
        public Dictionary<KeyColor, int> KeyInventory
        {
            get; set;
        }
        internal Dictionary<DurabilityType,int> Durabilities
        {
            get;set;
        }
        public int QuestItems { get; set; }
        public double TurnCounter { get; set; }

        private JObject ExportKeyInventories()
        {
            JObject export = new JObject();
            foreach(var key in KeyInventory.Keys)
            {
                export[key] = KeyInventory[key];
            }
            return export;
        }
        private JObject ExportDurabilities()
        {
            JObject export = new JObject();
            foreach (var key in Durabilities.Keys)
            {
                export[key] = Durabilities[key];
            }
            return export;
        }

        internal JToken Export()
        {
            JObject export = new JObject();
            export[Properties.Gold] = Gold;
            export[Properties.CreatureType] = (int)CreatureType;
            export[Properties.QuestItems] = QuestItems;
            export[Properties.TurnCounter] = TurnCounter;
            export[Properties.Durabilities] = ExportDurabilities();
            export[Properties.KeyInventory] = ExportKeyInventories();
            return export;
        }

        public CreatureInstance(MapCell<TGame> mapCell) 
            : base(mapCell)
        {
        }
    }
}
