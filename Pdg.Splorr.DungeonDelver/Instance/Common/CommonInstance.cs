﻿using Pdg.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;

namespace Pdg.Splorr.DungeonDelver
{
    public abstract class CommonInstance<TGame> where TGame : IGame<TGame>
    {
        private MapCell<TGame> _mapCell = null;
        virtual protected MapCell<TGame> GetMapCell()
        {
            return _mapCell;
        }
        internal MapCell<TGame> MapCell
        {
            get
            {
                return GetMapCell();
            }
            set
            {
                _mapCell = value;
            }
        }
        internal Map<TGame> Map { get { return MapCell.Map; } }
        internal Atlas<TGame> Atlas { get { return Map.Atlas; } }
        internal TGame Game { get { return Atlas.Game; } }
        public CommonInstance(MapCell<TGame> mapCell) 
        {
            MapCell = mapCell;
        }
    }
}
