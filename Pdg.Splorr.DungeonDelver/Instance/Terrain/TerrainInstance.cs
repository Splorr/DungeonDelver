﻿using Pdg.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;

namespace Pdg.Splorr.DungeonDelver
{
    public class TerrainInstance<TGame> : CommonInstance<TGame> where TGame : IGame<TGame>
    {
        private class Properties
        {
            public const string TerrainType = "terrainType";
            public const string KeyColor = "keyColor";
        }
        internal TerrainType TerrainType
        {
            get; set;
        }
        internal KeyColor? KeyColor
        {
            get; set;
        }
        public TerrainDescriptor<TGame> Descriptor
        {
            get
            {
                return Game.TerrainManager[TerrainType];
            }
        }

        internal JToken Export()
        {
            JObject export = new JObject();
            export[Properties.TerrainType] = (int)TerrainType;
            if(KeyColor.HasValue)
            {
                export[Properties.KeyColor] = (int)KeyColor.Value;
            }
            return export;
        }
        public TerrainInstance(MapCell<TGame> mapCell) 
            : base(mapCell)
        {
        }
    }
}
