﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pdg.Sdl2
{
    public enum EventType
    {
        Quit,
        KeyDown,
        KeyUp,
        User
    }
}
