﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pdg.Sdl2
{
    public struct WindowFlags
    {
        public bool FullScreen;
        public bool OpenGL;
        public bool Shown;
        public bool Hidden;
        public bool Borderless;
        public bool Resizable;
        public bool Minimized;
        public bool Maximized;
        public bool InputGrabbed;
        public bool InputFocus;
        public bool MouseFocus;
        public bool FullScreenDesktop;
        public bool Foreign;
        public bool AllowHighDPI;
        public bool MouseCapture;
    }
    public class Window : IDisposable
    {
        private IntPtr _ptr = IntPtr.Zero;

        internal IntPtr Pointer
        {
            get
            {
                return _ptr;
            }
        }
        private Window() { }
        public Window(string title, int? x, int? y, int w, int h, WindowFlags flags)
        {
            _ptr = SDL2.SDL.SDL_CreateWindow(title, x ?? SDL2.SDL.SDL_WINDOWPOS_CENTERED, y ?? SDL2.SDL.SDL_WINDOWPOS_CENTERED, w, h,
                ((flags.FullScreen) ? (SDL2.SDL.SDL_WindowFlags.SDL_WINDOW_FULLSCREEN) : (0)) |
                ((flags.AllowHighDPI) ? (SDL2.SDL.SDL_WindowFlags.SDL_WINDOW_ALLOW_HIGHDPI) : (0)) |
                ((flags.Borderless) ? (SDL2.SDL.SDL_WindowFlags.SDL_WINDOW_BORDERLESS) : (0)) |
                ((flags.Foreign) ? (SDL2.SDL.SDL_WindowFlags.SDL_WINDOW_FOREIGN) : (0)) |
                ((flags.FullScreenDesktop) ? (SDL2.SDL.SDL_WindowFlags.SDL_WINDOW_FULLSCREEN_DESKTOP) : (0)) |
                ((flags.Hidden) ? (SDL2.SDL.SDL_WindowFlags.SDL_WINDOW_HIDDEN) : (0)) |
                ((flags.InputFocus) ? (SDL2.SDL.SDL_WindowFlags.SDL_WINDOW_INPUT_FOCUS) : (0)) |
                ((flags.InputGrabbed) ? (SDL2.SDL.SDL_WindowFlags.SDL_WINDOW_INPUT_GRABBED) : (0)) |
                ((flags.Maximized) ? (SDL2.SDL.SDL_WindowFlags.SDL_WINDOW_MAXIMIZED) : (0)) |
                ((flags.Minimized) ? (SDL2.SDL.SDL_WindowFlags.SDL_WINDOW_MINIMIZED) : (0)) |
                ((flags.MouseCapture) ? (SDL2.SDL.SDL_WindowFlags.SDL_WINDOW_MOUSE_CAPTURE) : (0)) |
                ((flags.MouseFocus) ? (SDL2.SDL.SDL_WindowFlags.SDL_WINDOW_MOUSE_FOCUS) : (0)) |
                ((flags.OpenGL) ? (SDL2.SDL.SDL_WindowFlags.SDL_WINDOW_OPENGL) : (0)) |
                ((flags.Resizable) ? (SDL2.SDL.SDL_WindowFlags.SDL_WINDOW_RESIZABLE) : (0)) |
                ((flags.Shown) ? (SDL2.SDL.SDL_WindowFlags.SDL_WINDOW_SHOWN) : (0)));
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects).
                }

                if(_ptr!=IntPtr.Zero)
                {
                    SDL2.SDL.SDL_DestroyWindow(_ptr);
                    _ptr = IntPtr.Zero;
                }

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        ~Window()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(false);
        }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            GC.SuppressFinalize(this);
        }
        #endregion
    }
}
