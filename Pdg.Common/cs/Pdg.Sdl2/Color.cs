﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pdg.Sdl2
{
    public struct Color
    {
        public byte Red;
        public byte Green;
        public byte Blue;
        public byte Alpha;
        public Color(byte red, byte green, byte blue, byte alpha)
        {
            Red = red;
            Green = green;
            Blue = blue;
            Alpha = alpha;
        }
        internal Color(SDL2.SDL.SDL_Color color)
            : this(color.r, color.g, color.b, color.a)
        {

        }
    }
}
