﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pdg.Sdl2
{
    public struct SystemFlags
    {
        public bool Timer;
        public bool Audio;
        public bool Video;
        public bool Joystick;
        public bool Haptic;
        public bool GameController;
        public bool NoParachute;

        public static SystemFlags Everything
        {
            get
            {
                return new SystemFlags() { Timer = true, Audio = true, Video = true, Joystick = true, Haptic = true, GameController = true };
            }
        }
    }
    public struct ImageFlags
    {
        public bool Jpg;
        public bool Png;
        public bool Tif;
        public bool Webp;
    }
    public static class System
    {
        public static void Initialize(SystemFlags systemFlags,ImageFlags imageFlags)
        {
            SDL2.SDL.SDL_Init(
                ((systemFlags.Timer) ? (SDL2.SDL.SDL_INIT_TIMER) : (0)) +
                ((systemFlags.Audio) ? (SDL2.SDL.SDL_INIT_AUDIO) : (0)) +
                ((systemFlags.Video) ? (SDL2.SDL.SDL_INIT_VIDEO) : (0)) +
                ((systemFlags.Joystick) ? (SDL2.SDL.SDL_INIT_JOYSTICK) : (0)) +
                ((systemFlags.Haptic) ? (SDL2.SDL.SDL_INIT_HAPTIC) : (0)) +
                ((systemFlags.GameController) ? (SDL2.SDL.SDL_INIT_GAMECONTROLLER) : (0)) +
                ((systemFlags.NoParachute) ? (SDL2.SDL.SDL_INIT_NOPARACHUTE) : (0)));

            SDL2.SDL_image.IMG_Init(
                ((imageFlags.Jpg) ? (SDL2.SDL_image.IMG_InitFlags.IMG_INIT_JPG) : (0)) |
                ((imageFlags.Png) ? (SDL2.SDL_image.IMG_InitFlags.IMG_INIT_PNG) : (0)) |
                ((imageFlags.Tif) ? (SDL2.SDL_image.IMG_InitFlags.IMG_INIT_TIF) : (0)) |
                ((imageFlags.Webp) ? (SDL2.SDL_image.IMG_InitFlags.IMG_INIT_WEBP) : (0)));
        }
        public static void Quit()
        {
            SDL2.SDL_image.IMG_Quit();
            SDL2.SDL.SDL_Quit();
        }
    }
}
