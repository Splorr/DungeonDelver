﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pdg.Sdl2
{
    public class Surface: IDisposable
    {
        private IntPtr _ptr = IntPtr.Zero;

        internal IntPtr Pointer
        {
            get
            {
                return _ptr;
            }
        }

        private Surface() { }

        public Surface(string fileName)
        {
            _ptr = SDL2.SDL_image.IMG_Load(fileName);
            if(_ptr==IntPtr.Zero)
            {
                Debug.Print(SDL2.SDL.SDL_GetError());
            }
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                }

                if(_ptr!=IntPtr.Zero)
                {
                    SDL2.SDL.SDL_FreeSurface(_ptr);
                    _ptr = IntPtr.Zero;
                }

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        ~Surface()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(false);
        }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            GC.SuppressFinalize(this);
        }
        #endregion

    }
}
