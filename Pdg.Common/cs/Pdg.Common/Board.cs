﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pdg.Common
{
    public interface IBoard<TLocation,TCell> where TLocation:struct
    {
        TCell GetCell(TLocation location);
        void SetCell(TLocation location, TCell cell);
        bool HasCell(TLocation location);
        TCell this[TLocation location] { get; set; }
        IEnumerable<TLocation> Locations { get; }
    }
    public class Board<TLocation, TCell> : IBoard<TLocation, TCell> where TLocation : struct
    {
        private Dictionary<TLocation, TCell> _table = new Dictionary<TLocation, TCell>();
        private Func<TLocation, bool> _locationValidator;
        private Func<TCell> _cellDefault;
        public Board(Func<TLocation,bool> locationValidator, Func<TCell> cellDefault)
        {
            _locationValidator = locationValidator;
            _cellDefault = cellDefault;
        }
        public TCell this[TLocation location]
        {
            get
            {
                return GetCell(location);
            }

            set
            {
                SetCell(location, value);
            }
        }

        public IEnumerable<TLocation> Locations
        {
            get
            {
                return _table.Keys;
            }
        }

        public TCell GetCell(TLocation location)
        {
            if(!_locationValidator(location))
            {
                throw new InvalidOperationException("You attempted GetCell on an invalid location.");
            }
            TCell result;
            if(_table.TryGetValue(location, out result))
            {
                return result;
            }
            else
            {
                return _cellDefault();
            }
        }

        public bool HasCell(TLocation location)
        {
            return _table.ContainsKey(location);
        }

        public void SetCell(TLocation location, TCell cell)
        {
            if (!_locationValidator(location))
            {
                throw new InvalidOperationException("You attempted SetCell on an invalid location.");
            }
            if(HasCell(location))
            {
                _table[location] = cell;
            }
            else
            {
                _table.Add(location, cell);
            }
        }
    }
}
