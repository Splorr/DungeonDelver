﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pdg.Common
{
    public class XYCardinalWalker : CardinalWalker<XY>
    {
        public override XY Step(XY start, CardinalDirection direction)
        {
            switch(direction)
            {
                case CardinalDirection.East:
                    return new XY() { X = start.X+1, Y=start.Y };
                case CardinalDirection.North:
                    return new XY() { X = start.X, Y = start.Y - 1 };
                case CardinalDirection.South:
                    return new XY() { X = start.X, Y = start.Y + 1 };
                case CardinalDirection.West:
                    return new XY() { X = start.X - 1, Y = start.Y };
                default:
                    throw new InvalidOperationException("Called Step with an invalid direction.");
            }
        }
    }
}
