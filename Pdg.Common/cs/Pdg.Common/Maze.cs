﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pdg.Common
{
    public class Maze<TDirection, TLocation> : Board<TLocation, MazeCell<TDirection>> where TLocation:struct
    {
        private IWalker<TDirection, TLocation> _walker;
        public Maze(Func<TLocation, bool> locationValidator, IWalker<TDirection, TLocation> walker) 
            : base(locationValidator, ()=>new MazeCell<TDirection>())
        {
            _walker = walker;
        }

        public void Generate(int seedPoints, Func<int,int> randomizer)
        {
            var outside = new HashSet<TLocation>(Locations);
            var directions = _walker.Values;
            foreach(var location in outside)
            {
                this[location].Clear();
            }
            HashSet<TLocation> inside = new HashSet<TLocation>();
            HashSet<TLocation> frontier = new HashSet<TLocation>();
            while (seedPoints > 0)
            {
                TLocation current = outside.ToList()[randomizer(outside.Count)];
                inside.Add(current);
                outside.Remove(current);
                foreach (var direction in directions)
                {
                    var next = _walker.Walk(current, direction, 1);
                    if (outside.Contains(next))
                    {
                        outside.Remove(next);
                        frontier.Add(next);
                    }
                }
                seedPoints--;
            }
            while(frontier.Any())
            {
                var current = frontier.ToList()[randomizer(frontier.Count)];
                var validDirections = directions.Where(x=>inside.Contains(_walker.Walk(current,x,1)));
                if(validDirections.Any())
                {
                    var chosenDirection = validDirections.ToList()[randomizer(validDirections.Count())];
                    var next = _walker.Walk(current, chosenDirection, 1);
                    frontier.Remove(current);
                    inside.Add(current);
                    this[current].AddExit(chosenDirection);
                    this[next].AddExit(_walker.GetOpposite(chosenDirection));
                    foreach (var direction in directions)
                    {
                        next = _walker.Walk(current, direction, 1);
                        if (outside.Contains(next))
                        {
                            outside.Remove(next);
                            frontier.Add(next);
                        }
                    }
                }
            }

        }
    }
}
