﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Pdg.Common
{
    public interface IManager<TIdentifier, TManaged>: IDisposable
    {
        TManaged this[TIdentifier identifier]
        {
            get;
        }
    }
    public class Manager<TIdentifier, TManaged> : IManager<TIdentifier, TManaged>
    {
        private Dictionary<TIdentifier, TManaged> _table = new Dictionary<TIdentifier, TManaged>();
        private Func<TIdentifier, TManaged> _factory;
        private Action<TManaged> _cleanup;
        public Manager(Func<TIdentifier, TManaged> factory, Action<TManaged> cleanup)
        {
            _factory = factory;
            _cleanup = cleanup;
        }

        public TManaged this[TIdentifier identifier]
        {
            get
            {
                TManaged result;
                if(_table.TryGetValue(identifier, out result))
                {
                    return result;
                }
                else
                {
                    result = _factory(identifier);
                    _table.Add(identifier, result);
                    return result;
                }
            }
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    _table.Values.ToList().ForEach(_cleanup);
                }

                disposedValue = true;
            }
        }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
        }
        #endregion
    }
}
