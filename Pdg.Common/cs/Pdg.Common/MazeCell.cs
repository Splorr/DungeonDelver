﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pdg.Common
{
    public class MazeCell<TDirection>
    {
        private HashSet<TDirection> _directions = new HashSet<TDirection>();
        public bool HasExit(TDirection direction)
        {
            return _directions.Contains(direction);
        }
        public void AddExit(TDirection direction)
        {
            _directions.Add(direction);
        }
        public void RemoveExit(TDirection direction)
        {
            _directions.Remove(direction);
        }
        public void Clear()
        {
            _directions.Clear();
        }
        public IEnumerable<TDirection> Exits
        {
            get
            {
                return _directions;
            }
        }
    }
}
