﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pdg.Common
{
    public class XYOrdinalWalker : OrdinalWalker<XY>
    {
        public override XY Step(XY start, OrdinalDirection direction)
        {
            switch (direction)
            {
                case OrdinalDirection.East:
                    return new XY() { X = start.X + 1, Y = start.Y };
                case OrdinalDirection.North:
                    return new XY() { X = start.X, Y = start.Y - 1 };
                case OrdinalDirection.NorthEast:
                    return new XY() { X = start.X + 1, Y = start.Y - 1 };
                case OrdinalDirection.NorthWest:
                    return new XY() { X = start.X - 1, Y = start.Y - 1 };
                case OrdinalDirection.South:
                    return new XY() { X = start.X, Y = start.Y + 1 };
                case OrdinalDirection.SouthEast:
                    return new XY() { X = start.X + 1, Y = start.Y + 1 };
                case OrdinalDirection.SouthWest:
                    return new XY() { X = start.X - 1, Y = start.Y + 1 };
                case OrdinalDirection.West:
                    return new XY() { X = start.X - 1, Y = start.Y };
                default:
                    throw new InvalidOperationException("Called Step with an invalid direction.");
            }
        }
    }
}
