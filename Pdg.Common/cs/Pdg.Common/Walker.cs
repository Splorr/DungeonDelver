﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pdg.Common
{
    public interface IWalker<TDirection,TLocation>
    {
        IEnumerable<TDirection> Values { get; }
        TDirection GetOpposite(TDirection direction);
        TLocation Walk(TLocation start, TDirection direction, int steps);
        TLocation Step(TLocation start, TDirection direction);
    }
    public abstract class Walker<TDirection, TLocation>
        : IWalker<TDirection, TLocation>
    {
        public abstract IEnumerable<TDirection> Values { get; }

        public abstract TDirection GetOpposite(TDirection direction);
        public abstract TLocation Step(TLocation start, TDirection direction);
        public TLocation Walk(TLocation start, TDirection direction, int steps)
        {
            if(steps<0)
            {
                return Walk(start, GetOpposite(direction), -steps);
            }
            else
            {
                while(steps>0)
                {
                    start = Step(start, direction);
                    steps--;
                }
                return start;
            }
        }
    }
}
