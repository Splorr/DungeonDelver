﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pdg.Common
{
    public interface IDescriptor<TIdentifier>
    {
        void SetProperty<TProperty>(TIdentifier identifier, TProperty property);
        TProperty GetProperty<TProperty>(TIdentifier identifier);
        bool HasProperty(TIdentifier identifier);
        IEnumerable<TIdentifier> Identifiers { get; }
    }
    public class Descriptor<TIdentifier> : IDescriptor<TIdentifier>
    {
        private Dictionary<TIdentifier, object> _properties = new Dictionary<TIdentifier, object>();
        private Func<Descriptor<TIdentifier>, TIdentifier, Type, bool> _propertyValidator;
        private Func<Descriptor<TIdentifier>, TIdentifier, Type, object> _propertyDefault;
        public Descriptor(
            Func<Descriptor<TIdentifier>, TIdentifier, Type, bool> propertyValidator,
            Func<Descriptor<TIdentifier>, TIdentifier, Type, object> propertyDefault)
        {
            _propertyValidator = propertyValidator;
            _propertyDefault = propertyDefault;
        }
        public IEnumerable<TIdentifier> Identifiers
        {
            get
            {
                return _properties.Keys;
            }
        }

        public TProperty GetProperty<TProperty>(TIdentifier identifier)
        {
            if (!_propertyValidator(this, identifier, typeof(TProperty)))
            {
                throw new InvalidOperationException("Attempted to call GetProperty with invalid property type for identifier.");
            }
            if (HasProperty(identifier))
            {
                return (TProperty)_properties[identifier];
            }
            else
            {
                return (TProperty)_propertyDefault(this, identifier, typeof(TProperty));
            }
        }

        public bool HasProperty(TIdentifier identifier)
        {
            return _properties.ContainsKey(identifier);
        }

        public void SetProperty<TProperty>(TIdentifier identifier, TProperty property)
        {
            if(!_propertyValidator(this,identifier,typeof(TProperty)))
            {
                throw new InvalidOperationException("Attempted to call SetProperty with invalid property type for identifier.");
            }
            if(HasProperty(identifier))
            {
                _properties[identifier] = property;
            }
            else
            {
                _properties.Add(identifier, property);
            }
        }
        protected TProperty DefaultedGetProperty<TProperty>(TIdentifier property, TProperty defaultValue)
        {
            if (HasProperty(property))
            {
                return GetProperty<TProperty>(property);
            }
            else
            {
                return defaultValue;
            }
        }
        protected TProperty? DefaultedGetProperty<TProperty>(TIdentifier property) where TProperty : struct
        {
            if (HasProperty(property))
            {
                return GetProperty<TProperty>(property);
            }
            else
            {
                return null;
            }
        }
        protected TResult DefaultedCallProperty<TResult>(TIdentifier property, TResult defaultValue)
        {
            if (HasProperty(property))
            {
                return GetProperty<Func<TResult>>(property)();
            }
            else
            {
                return defaultValue;
            }
        }
        protected TResult? DefaultedCallProperty<TResult>(TIdentifier property) where TResult:struct
        {
            if (HasProperty(property))
            {
                return GetProperty<Func<TResult>>(property)();
            }
            else
            {
                return null;
            }
        }
        protected TResult DefaultedCallProperty<TFirst, TResult>(TIdentifier property, TFirst first, TResult defaultValue)
        {
            if (HasProperty(property))
            {
                return GetProperty<Func<TFirst, TResult>>(property)(first);
            }
            else
            {
                return defaultValue;
            }
        }
        protected TResult? DefaultedCallProperty<TFirst, TResult>(TIdentifier property, TFirst first) where TResult:struct
        {
            if (HasProperty(property))
            {
                return GetProperty<Func<TFirst, TResult>>(property)(first);
            }
            else
            {
                return null;
            }
        }
        protected TResult DefaultedCallProperty<TFirst, TSecond, TResult>(TIdentifier property, TFirst first, TSecond second, TResult defaultValue)
        {
            if (HasProperty(property))
            {
                return GetProperty<Func<TFirst, TSecond, TResult>>(property)(first, second);
            }
            else
            {
                return defaultValue;
            }
        }
        protected TResult? DefaultedCallProperty<TFirst, TSecond, TResult>(TIdentifier property, TFirst first, TSecond second) where TResult:struct
        {
            if (HasProperty(property))
            {
                return GetProperty<Func<TFirst, TSecond, TResult>>(property)(first, second);
            }
            else
            {
                return null;
            }
        }
        protected void OptionalActionProperty(TIdentifier property)
        {
            if (HasProperty(property))
            {
                GetProperty<Action>(property)();
            }
        }
        protected void OptionalActionProperty<TFirst>(TIdentifier property, TFirst first)
        {
            if (HasProperty(property))
            {
                GetProperty<Action<TFirst>>(property)(first);
            }
        }
        protected void OptionalActionProperty<TFirst, TSecond>(TIdentifier property, TFirst first, TSecond second)
        {
            if (HasProperty(property))
            {
                GetProperty<Action<TFirst, TSecond>>(property)(first, second);
            }
        }

    }
}
