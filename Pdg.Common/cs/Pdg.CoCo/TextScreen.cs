﻿using Pdg.Sdl2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pdg.CoCo
{
    public class TextScreen: IDisposable
    {
        public const int CellColumns = 32;
        public const int CellRows = 16;
        private const int CellWidth = 16;
        private const int CellHeight = 24;
        private const int CharacterCount = 256;
        private const int ScreenWidth = CellColumns * CellWidth;
        private const int ScreenHeight = CellRows * CellHeight;
        private const int CellCount = CellColumns * CellRows;
        private const byte ClearCharacter = 96;
        private const byte CursorStart = 0x8f;
        private const byte CursorAdd = 0x10;
        private const byte CursorOr = 0x80;

        private Renderer _renderer;
        private Texture _texture;
        private Rectangle[] _sourceRectangles;
        private Rectangle[] _destinationRectangles;
        private byte[] _characters;
        private int _cursor;
        private byte _cursorColor;

        public int Cursor
        {
            get
            {
                return _cursor;
            }
            set
            {
                _cursor = value % CellCount;
                if(_cursor<0)
                {
                    _cursor += CellCount;
                }
            }
        }
        public int CursorColumn
        {
            get
            {
                return Cursor % CellColumns;
            }
            set
            {
                int row = CursorRow;
                Cursor = row * CellColumns + value;
            }
        }
        public int CursorRow
        {
            get
            {
                return Cursor / CellColumns;
            }
            set
            {
                int column = CursorColumn;
                Cursor = value * CellColumns + column;
            }
        }

        public TextScreen(Renderer renderer, string fileName)
        {
            _renderer = renderer;
            _texture = new Texture(_renderer,fileName);
            _sourceRectangles = new Rectangle[CharacterCount];
            for(int index=0;index< CharacterCount; ++index)
            {
                int column = index % CellColumns;
                int row = index / CellColumns;
                _sourceRectangles[index] = new Rectangle(column * CellWidth, row * CellHeight, CellWidth, CellHeight);
            }
            _destinationRectangles = new Rectangle[CellCount];
            for (int index = 0; index < CellCount; ++index)
            {
                int column = index % CellColumns;
                int row = index / CellColumns;
                _destinationRectangles[index] = new Rectangle(column * CellWidth, row * CellHeight, CellWidth, CellHeight);
            }
            _characters = new byte[CellCount];
            _cursor = 0;
            _cursorColor = CursorStart;
        }
        public void ResetRenderer()
        {
            _renderer.LogicalSize = new Rectangle(0, 0, ScreenWidth, ScreenHeight);
        }

        public void Render()
        {
            for(int index=0;index<CellCount;++index)
            {
                _renderer.Copy(_destinationRectangles[index], _texture, _sourceRectangles[_characters[index]]);
            }
        }

        public void ShowCursor()
        {
            _characters[_cursor] = _cursorColor;
            _cursorColor += CursorAdd;
            _cursorColor |= CursorOr;
        }

        public void HideCursor()
        {
            _characters[_cursor] = ClearCharacter;
        }

        public void Clear()
        {
            for(int index=0;index<CellCount;++index)
            {
                _characters[index] = ClearCharacter;
            }
            _cursor = 0;
        }

        private void ScrollScreen()
        {
            for(int index=0;index<CellCount-CellColumns;++index)
            {
                _characters[index] = _characters[index + CellColumns];
            }
            for(int index=CellCount-CellColumns;index<CellCount;++index)
            {
                _characters[index] = ClearCharacter;
            }
        }

        private void AdvanceCursor(bool wrapCursor)
        {
            _cursor++;
            if(_cursor >= CellCount)
            {
                if (wrapCursor)
                {
                    _cursor -= CellCount;
                }
                else
                {
                    ScrollScreen();
                    _cursor -= CellColumns;
                }
            }
        }

        public void Write(byte value, bool wrapCursor)
        {
            _characters[_cursor] = value;
            AdvanceCursor(wrapCursor);
        }

        public void Write(char value, bool wrapCursor)
        {
            if(value>=32 && value<64)
            {
                Write((byte)(value + 64), wrapCursor);
            }
            else if (value >= 64 && value < 96)
            {
                Write((byte)(value), wrapCursor);
            }
            else if (value >= 96 && value < 128)
            {
                Write((byte)(value-96), wrapCursor);
            }
            else
            {
                Write(0, wrapCursor);
            }
        }

        public void Write(string value, bool wrapCursor)
        {
            foreach(var character in value)
            {
                Write(character, wrapCursor);
            }
        }

        public void NewLine(bool wrapCursor)
        {
            if(_cursor%CellColumns==0)
            {
                Write(ClearCharacter, wrapCursor);
            }
            while (_cursor% CellColumns != 0)
            {
                Write(ClearCharacter, wrapCursor);
            }
        }

        public void WriteLine(string value, bool wrapCursor)
        {
            Write(value, wrapCursor);
            NewLine(wrapCursor);
        }

        public void Dispose()
        {
            ((IDisposable)_texture).Dispose();
        }
    }
}
