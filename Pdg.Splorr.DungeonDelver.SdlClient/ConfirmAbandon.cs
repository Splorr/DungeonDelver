﻿using Pdg.CoCo;
using Pdg.Sdl2;

namespace Pdg.Splorr.DungeonDelver.SdlClient
{
    internal static class ConfirmAbandon
    {
        internal static bool? Run(Renderer renderer, TextScreen textScreen)
        {
            textScreen.CursorRow = 7;
            textScreen.CursorColumn = 2;
            textScreen.Write("ABANDON GAME? (Y/N)", true);
            return Utility.StandardEventHandler<bool?>(
                () => 
                {
                    renderer.Clear();
                    textScreen.Render();
                    renderer.Present();
                }, 
                null, 
                (k, r) => 
                {
                    switch (k.KeyCode)
                    {
                        case KeyCode.N:
                            r(false);
                            break;
                        case KeyCode.Y:
                            r(true);
                            break;
                    }
                });
        }
    }
}
