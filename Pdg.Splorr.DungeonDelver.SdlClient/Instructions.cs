﻿using Pdg.CoCo;
using Pdg.Sdl2;

namespace Pdg.Splorr.DungeonDelver.SdlClient
{
    internal static class Instructions
    {
        const int Pages = 2;
        private static void Redraw(TextScreen textScreen, int page)
        {
            textScreen.Clear();
            textScreen.WriteLine(string.Format("INSTRUCTIONS (PAGE {0} OF {1})", page+1, Pages), false);

            switch(page)
            {
                case 0:
                    textScreen.WriteLine("object", true);
                    //                    01234567890123456789012345678901  
                    textScreen.WriteLine("COLLECT ALL OF THE QUEST ITEMS", true);
                    textScreen.WriteLine("(Q), FIND THE RAINBOW KEY ( ),", true);//TODO: put rainbow key image here
                    textScreen.WriteLine("THEN GET TO THE EXIT ( ).", true);//TODO: put exit image here
                    textScreen.WriteLine("", true);
                    textScreen.WriteLine("controls", true);
                    textScreen.WriteLine("{arrows} MOVE", true);
                    textScreen.WriteLine("{m} MAP", true);
                    textScreen.WriteLine("{tab} EQUIP MODE/INVENTORY", true);
                    textScreen.WriteLine("{y} USE AMULET", true);
                    textScreen.WriteLine("{e}AT FOOD", true);
                    textScreen.WriteLine("{q}UAFF POTION", true);
                    textScreen.WriteLine("{esc} SAVE/ABANDON GAME", true);
                    textScreen.WriteLine("", true);
                    break;
                case 1:
                    textScreen.WriteLine("terrain", true);
                    //                    01234567890123456789012345678901  
                    textScreen.Write("WALLS ", true);
                    textScreen.Write(0x80, true);
                    textScreen.WriteLine("", true);
                    textScreen.Write("FLOOR ", true);
                    textScreen.Write(0x20, true);
                    textScreen.WriteLine("", true);
                    textScreen.Write("DOORS ", true);
                    textScreen.Write(0x89, true);
                    textScreen.Write(0x99, true);
                    textScreen.Write(0xA9, true);
                    textScreen.Write(0xB9, true);
                    textScreen.Write(0xC9, true);
                    textScreen.Write(0xD9, true);
                    textScreen.Write(0xE9, true);
                    textScreen.Write(0xF9, true);
                    textScreen.WriteLine("", true);
                    textScreen.WriteLine("TRAPS t", true);
                    break;
            }

            textScreen.CursorRow = 15;
            textScreen.Write("{esc} MENU {arrows} NEXT/PREV", true);

        }
        internal static bool Run(Renderer renderer, TextScreen textScreen)
        {
            int page = 0;
            return Utility.StandardEventHandler(
                () =>
                {
                    Redraw(textScreen, page);
                    renderer.Clear();
                    textScreen.Render();
                    renderer.Present();
                },
                false,
                (k, r) =>
                {
                    switch (k.KeyCode)
                    {
                        case KeyCode.Escape:
                            r(true);
                            break;
                        case KeyCode.Left:
                            page = (page + Pages - 1) % Pages;
                            break;
                        case KeyCode.Right:
                            page = (page + 1) % Pages;
                            break;
                    }
                });
        }
    }
}
