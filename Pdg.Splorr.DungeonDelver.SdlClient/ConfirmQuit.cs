﻿using Pdg.CoCo;
using Pdg.Sdl2;

namespace Pdg.Splorr.DungeonDelver.SdlClient
{
    internal static class ConfirmQuit
    {
        internal static bool? Run(Renderer renderer, TextScreen textScreen)
        {
            textScreen.Clear();
            textScreen.WriteLine("ARE YOU SURE?", false);
            textScreen.WriteLine("", false);
            textScreen.WriteLine("{y}ES", false);
            textScreen.WriteLine("{n}O", false);
            return Utility.StandardEventHandler<bool?>(
                () => 
                {
                    renderer.Clear();
                    textScreen.Render();
                    renderer.Present();
                }, 
                null, 
                (k, r) => 
                {
                    switch (k.KeyCode)
                    {
                        case KeyCode.N:
                            r(false);
                            break;
                        case KeyCode.Y:
                            r(true);
                            break;
                    }
                });
        }
    }
}
