﻿using Pdg.CoCo;
using Pdg.Sdl2;

namespace Pdg.Splorr.DungeonDelver.SdlClient
{
    internal static class MainMenu
    {
        internal static void Redraw(TextScreen textScreen)
        {
            textScreen.Clear();
            textScreen.WriteLine("SPLORR!! DUNGEON DELVER", false);
            textScreen.WriteLine("",false);
            textScreen.WriteLine("{n}EW GAME", false);
            textScreen.WriteLine("{l}OAD GAME", false);
            textScreen.WriteLine("{i}NSTRUCTIONS", false);
            textScreen.WriteLine("{o}PTIONS", false);
            textScreen.WriteLine("{a}BOUT", false);
            textScreen.WriteLine("{q}UIT", false);
        }
        internal static void Run(Renderer renderer, TextScreen textScreen)
        {
            Redraw(textScreen);
            bool done = false;
            while(!done)
            {
                renderer.Clear();
                textScreen.Render();
                renderer.Present();
                Event evt = Event.Poll();
                if (evt != null)
                {
                    if (evt.Type == EventType.Quit)
                    {
                        done = true;
                    }
                    else if (evt.Type == EventType.KeyDown)
                    {
                        KeyEvent keyEvent = evt as KeyEvent;
                        switch(keyEvent.KeyCode)
                        {
                            case KeyCode.N:
                                var difficulty = ChooseDifficulty.Run(renderer, textScreen);
                                if(difficulty.HasValue)
                                {
                                    Game game = new Game(difficulty.Value);
                                    game.Reset();
                                    if(!Play.Run(game, renderer, textScreen))
                                    {
                                        done = true;
                                    }
                                    else
                                    {
                                        Redraw(textScreen);
                                    }
                                }
                                else
                                {
                                    done = true;
                                }
                                break;
                            case KeyCode.L:
                                break;
                            case KeyCode.I:
                                if(!Instructions.Run(renderer, textScreen))
                                {
                                    done = true;
                                }
                                else
                                {
                                    Redraw(textScreen);
                                }
                                break;
                            case KeyCode.O:
                                if (!Options.Run(renderer, textScreen))
                                {
                                    done = true;
                                }
                                else
                                {
                                    Redraw(textScreen);
                                }
                                break;
                            case KeyCode.A:
                                if (!About.Run(renderer, textScreen))
                                {
                                    done = true;
                                }
                                else
                                {
                                    Redraw(textScreen);
                                }
                                break;
                            case KeyCode.Q:
                                bool? confirm = ConfirmQuit.Run(renderer, textScreen);
                                if(confirm.HasValue)
                                {
                                    if(confirm.Value)
                                    {
                                        done = true;
                                    }
                                    else
                                    {
                                        Redraw(textScreen);
                                    }
                                }
                                else
                                {
                                    done = true;
                                }
                                break;
                        }
                    }
                }
            }
        }
    }
}
