﻿using Pdg.CoCo;
using Pdg.Sdl2;

namespace Pdg.Splorr.DungeonDelver.SdlClient
{
    internal static class Options
    {
        private static void Redraw(TextScreen textScreen)
        {
            textScreen.Clear();
            textScreen.WriteLine("OPTIONS", false);
            textScreen.CursorRow = 15;
            textScreen.Write("{esc} GO BACK", true);

        }
        internal static bool Run(Renderer renderer, TextScreen textScreen)
        {
            return Utility.StandardEventHandler(
                () =>
                {
                    Redraw(textScreen);
                    renderer.Clear();
                    textScreen.Render();
                    renderer.Present();
                },
                false,
                (k, r) =>
                {
                    switch (k.KeyCode)
                    {
                        case KeyCode.Escape:
                            r(true);
                            break;
                    }
                });
        }
    }
}
